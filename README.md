# `signalctx`

[![Go
Reference](https://pkg.go.dev/badge/gitlab.com/matthewhughes/signalctx.svg)](https://pkg.go.dev/gitlab.com/matthewhughes/signalctx)

Replacement for
[`os/signal.NotifyContext`](https://pkg.go.dev/os/signal#NotifyContext) that
allows you to inspect the signal that stopped context.
