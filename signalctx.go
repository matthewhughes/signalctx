package signalctx

import (
	"context"
	"errors"
	"fmt"
	"os"
	"os/signal"
)

type NotifiedError struct {
	Signal os.Signal
}

func (e *NotifiedError) Error() string {
	return fmt.Sprintf("received signal: %s", e.Signal)
}

// FromContext returns the [NotifiedError] that is the cause for ctx to be
// done, or nil if ctx is not done or the cause was any other type of error.
// It's basically a convenience wrapper around [errors.As].
func FromContext(ctx context.Context) *NotifiedError {
	var err *NotifiedError
	if errors.As(context.Cause(ctx), &err) {
		return err
	}
	return nil
}

type signalCtx struct {
	context.Context //nolint:containedctx

	cancelCause context.CancelCauseFunc
	ch          chan os.Signal
}

func (c *signalCtx) stop() {
	c.cancelCause(nil)
	signal.Stop(c.ch)
}

func (c *signalCtx) notify(signal os.Signal) {
	err := &NotifiedError{Signal: signal}
	c.cancelCause(err)
}

// NotifyContext is like [signal.NotifyContext] except that if a matching
// signal arrives the cause of the context will be set to a [NotifiedError].
// The returned [context.CancelFunc] behaves per the cancel function from
// [signal.NotifyContext].
func NotifyContext(
	parent context.Context,
	signals ...os.Signal,
) (context.Context, context.CancelFunc) {
	ctx, cancelCause := context.WithCancelCause(parent)
	c := &signalCtx{
		Context:     ctx,
		cancelCause: cancelCause,
		ch:          make(chan os.Signal, 1),
	}

	signal.Notify(c.ch, signals...)
	if ctx.Err() == nil {
		go func() {
			select {
			case signal := <-c.ch:
				c.notify(signal)
			case <-c.Done():
			}
		}()
	}

	return c, c.stop
}
