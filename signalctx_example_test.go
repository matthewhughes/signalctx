//nolint:testableexamples // it's painful to run example tests with signals
package signalctx_test

import (
	"context"
	"errors"
	"fmt"
	"os"

	"gitlab.com/matthewhughes/signalctx"
)

func ExampleNotifyContext() {
	ctx, cancel := signalctx.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	// do some work that may be interrupted

	var err *signalctx.NotifiedError
	if errors.As(context.Cause(ctx), &err) {
		fmt.Printf("received signal: %s\n", err.Signal)
	} else {
		fmt.Println("never saw a signal")
	}
}

func ExampleFromContext() {
	ctx, cancel := signalctx.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	// do some work that may be interrupted

	if err := signalctx.FromContext(ctx); err != nil {
		fmt.Printf("received signal: %s\n", err.Signal)
	} else {
		fmt.Println("never saw a signal")
	}
}
