//go:build linux || darwin

package signalctx_test

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/matthewhughes/signalctx"
)

func TestNotifyContext_CapturesSignal(t *testing.T) {
	for _, signal := range []syscall.Signal{syscall.SIGINT, syscall.SIGTERM} {
		t.Run(signal.String(), func(t *testing.T) {
			ctx, cancel := signalctx.NotifyContext(context.Background(), signal)
			defer cancel()
			expectedErr := "received signal: " + signal.String()

			require.NoError(t, syscall.Kill(syscall.Getpid(), signal))

			var err *signalctx.NotifiedError
			assert.Eventually(
				t,
				func() bool { return ctx.Err() != nil },
				time.Second,
				10*time.Millisecond,
			)
			assert.ErrorAs(t, context.Cause(ctx), &err) //nolint:testifylint
			assert.Equal(t, signal, err.Signal)
			assert.EqualError(t, err, expectedErr)
		})
	}
}

func TestNoifyContext_NotInteractionWithOtherSignals(t *testing.T) {
	sentSignal := syscall.SIGTERM
	otherSignal := syscall.SIGINT

	ctx := context.Background()
	notifiedCtx, cancelSent := signal.NotifyContext(ctx, sentSignal)
	sigCtx, cancelOther := signalctx.NotifyContext(ctx, otherSignal)
	defer cancelSent()
	defer cancelOther()

	require.NoError(t, syscall.Kill(syscall.Getpid(), sentSignal))

	require.Eventually(
		t,
		func() bool { return notifiedCtx.Err() != nil },
		time.Second,
		10*time.Millisecond,
	)
	require.NoError(t, sigCtx.Err())
}

func TestNoifyContext_CancelResetsHandling(t *testing.T) {
	ctx := context.Background()
	sig := os.Interrupt

	sigCtx, cancelSig := signalctx.NotifyContext(ctx, sig)

	cancelSig()
	require.ErrorIs(t, sigCtx.Err(), context.Canceled)
	require.ErrorIs(t, context.Cause(sigCtx), context.Canceled)

	notifiedCtx, cancel := signal.NotifyContext(ctx, sig)
	defer cancel()
	sysSig, ok := sig.(syscall.Signal)
	require.True(t, ok)
	require.NoError(t, syscall.Kill(syscall.Getpid(), sysSig))

	require.Eventually(
		t,
		func() bool { return notifiedCtx.Err() != nil },
		time.Second,
		10*time.Millisecond,
	)
}

func TestFromContext_ReturnsNilOnNoError(t *testing.T) {
	ctx, cancel := signalctx.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	require.Nil(t, signalctx.FromContext(ctx))
}

func TestFromContext_ReturnsNilOnOtherContext(t *testing.T) {
	ctx := context.Background()

	require.Nil(t, signalctx.FromContext(ctx))
}

func TestFromContext_ReturnsNotifiedError(t *testing.T) {
	sig := syscall.SIGINT
	ctx, cancel := signalctx.NotifyContext(context.Background(), sig)
	defer cancel()
	expected := &signalctx.NotifiedError{Signal: sig}

	require.NoError(t, syscall.Kill(syscall.Getpid(), sig))

	require.Eventually(t, func() bool { return ctx.Err() != nil }, time.Second, 10*time.Millisecond)
	require.Equal(t, expected, signalctx.FromContext(ctx))
}
